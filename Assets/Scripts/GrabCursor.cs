using UnityEngine;
using System;

public class GrabCursor : AbstractCursor
{
    private MeshRenderer _renderer;

    // Start is called before the first frame update
    void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
        _renderer.enabled = false;
    }

    public override KeyCode KeyCode()
    {
        return UnityEngine.KeyCode.G;
    }

    public override void Place()
    {
        GrabbableObject  def = _lastRaycastHit.collider.GetComponent<GrabbableObject>();
        _renderer.enabled = def == null ? false : true;
    }

    public override void Use()
    {
        GrabbableObject def = _lastRaycastHit.collider.GetComponent<GrabbableObject>();
        if (def == null) return;
        if (!def.Grabbed) def.Grab();
        else def.Ungrab();
    }

    private void OnDisable()
    {
        _renderer.enabled = false;
    }
}
