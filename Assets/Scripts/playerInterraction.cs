using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class playerInterraction : MonoBehaviour
{
    public GameObject box;
    public GameObject boxCanvas;
    public Camera playerCamera;

    // Update is called once per frame
    void Update()
    {
        //Condition a remplacer par isServed
        if (box.activeSelf == true)
            boxCanvas.transform.LookAt(playerCamera.transform.position);

        if (Input.GetKeyDown(KeyCode.E))
        {
            if (box.activeSelf == false)
            {
                box.SetActive(true);
                boxCanvas.transform.LookAt(playerCamera.transform.position);
            }
            StartCoroutine(imageCooldown());
        }
    }

    IEnumerator imageCooldown()
    {
        yield return new WaitForSeconds(3);
        if (box.activeSelf == true)
            box.SetActive(false);
    }
}
