using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraMovement : MonoBehaviour
{
    public GameObject Player;
    public float mouseSensitivity = 100;

    private float horizontal;
    private float vertical;
    private float Rotation = 0;
    private Rigidbody rb;

    private float xMove, yMove;
    private float PlayerSpeed = 5f;

    void Start()
    {
        rb = Player.GetComponent<Rigidbody>();
        Cursor.lockState = CursorLockMode.Locked;
    }
    void Update()
    {
        if(Cursor.lockState == CursorLockMode.Locked)
            CameraControl();

    }

    void CameraControl()
    {
        horizontal = Input.GetAxis("Mouse X") * mouseSensitivity * Time.deltaTime;
        vertical = Input.GetAxis("Mouse Y") * mouseSensitivity * Time.deltaTime;

        Rotation -= vertical;
        Rotation = Mathf.Clamp(Rotation, -90f, 75f);

        transform.localRotation = Quaternion.Euler(Rotation, 0f, 0f);
        Player.transform.Rotate(Vector3.up * horizontal);
    }
}
