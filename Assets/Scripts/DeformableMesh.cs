using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class DeformableMesh : MonoBehaviour
{
    private MeshFilter _meshFilter;
    private MeshCutter _meshCutter;

    private Material _materialInstance;

    [SerializeField]
    private Color _baseColor;

    // Start is called before the first frame update
    void Start()
    {
        _meshFilter = GetComponent<MeshFilter>();
        _meshCutter = new MeshCutter(256);

        MeshRenderer renderer = GetComponent<MeshRenderer>();
        _materialInstance = Instantiate(Resources.Load("DeformableMeshMaterial", typeof(Material)) as Material);
        renderer.material = _materialInstance;
    }

    /*public void Deform(Vector3 point, Vector3 direction, float power)
    {
        Deform(_meshFilter.mesh, point, direction, power);
        _meshCollider.sharedMesh = _meshFilter.mesh;
    }*/

    /*private void Deform(Mesh mesh, Vector3 point, Vector3 direction, float power)
    {
        List<Vector3> vertices = new List<Vector3>(mesh.vertices);
        
        for (int i = 0; i < vertices.Count; i++)
        {
            Vector3 vertex = vertices[i];
            Vector3 pointToVertex = vertex - point;
            pointToVertex.x *= transform.localScale.x;
            pointToVertex.y *= transform.localScale.y;
            pointToVertex.z *= transform.localScale.z;

            float distance = pointToVertex.sqrMagnitude * 5;
            Vector3 displacement = Vector3.zero;
            displacement.x = direction.x * power / ((1 + distance * distance) * 10);
            displacement.y = direction.y * power / ((1 + distance * distance) * 10);
            displacement.z = direction.z * power / ((1 + distance * distance) * 10);
            vertices[i] += displacement;
        }

        mesh.SetVertices(vertices.ToArray());
        mesh.RecalculateNormals();
        mesh.RecalculateTangents();
    }*/

    public void Cut(Vector3 point, Vector3 normal)
    {
        Vector3 transformedNormal = ((Vector3)(transform.localToWorldMatrix.transpose * normal)).normalized;
        Plane plane = new Plane();
        plane.SetNormalAndPosition(transformedNormal, transform.InverseTransformPoint(point));
        if (!_meshCutter.SliceMesh(_meshFilter.mesh, ref plane))
        {
            Debug.LogWarning("Aucun objet issue du slice");
            return;
        }
        GameObject newObj = Instantiate(gameObject);
        newObj.transform.SetPositionAndRotation(transform.position, transform.rotation);

        ReplaceMesh(_meshFilter.mesh, _meshCutter.PositiveMesh, GetComponent<MeshCollider>());
        ReplaceMesh(newObj.GetComponent<MeshFilter>().mesh, _meshCutter.NegativeMesh, newObj.GetComponent<MeshCollider>());
    }

    public void Highlight()
    {
        _materialInstance.color = _baseColor * 1.25f;
    }

    public void Unhighlight()
    {
        _materialInstance.color = _baseColor;
    }

    void ReplaceMesh(Mesh mesh, TempMesh tempMesh, MeshCollider collider = null)
    {
        mesh.Clear();
        mesh.SetVertices(tempMesh.vertices);
        mesh.SetTriangles(tempMesh.triangles, 0);
        mesh.SetNormals(tempMesh.normals);
        mesh.SetUVs(0, tempMesh.uvs);

        //mesh.RecalculateNormals();
        mesh.RecalculateTangents();

        if (collider != null && collider.enabled)
        {
            collider.sharedMesh = mesh;
            collider.convex = true;
        }
    }

}
