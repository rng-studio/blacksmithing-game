using UnityEngine;

public class CharacterAnimationInterface : MonoBehaviour
{
    private const string DO_HAPPY_GESTURE_STRING = "doHappyHandGesture";
    private const string DO_ANGRY_GESTURE_STRING = "doAngryGesture";
    private const string START_WALKING_STRING = "startWalking";
    private const string STOP_WALKING_STRING = "stopWalking";
    private const string START_TALKING_STRING = "startTalking";
    private const string STOP_TALKING_STRING = "stopTalking";

    private Animator _animator;

    // Start is called before the first frame update
    void Start()
    {
        _animator = GetComponent<Animator>();
        if (_animator == null)
            _animator = GetComponentInChildren<Animator>();
    }

    public void Walk()
    {
        _animator.ResetTrigger(STOP_WALKING_STRING);
        _animator.SetTrigger(START_WALKING_STRING);
    }

    public void StopWalk()
    {
        _animator.SetTrigger(STOP_WALKING_STRING);
    }

    public void Talk()
    {
        _animator.ResetTrigger(STOP_TALKING_STRING);
        _animator.SetTrigger(START_TALKING_STRING);
    }

    public void StopTalk()
    {
        _animator.SetTrigger(STOP_TALKING_STRING);
    }

    public void DoHappyGesture()
    {
        _animator.SetTrigger(DO_HAPPY_GESTURE_STRING);
        _animator.SetTrigger(START_WALKING_STRING);
        _animator.SetTrigger(START_TALKING_STRING);
    }

    public void DoAngryGesture()
    {
        _animator.SetTrigger(DO_ANGRY_GESTURE_STRING);
        _animator.SetTrigger(START_WALKING_STRING);
        _animator.SetTrigger(START_TALKING_STRING);
    }
}
