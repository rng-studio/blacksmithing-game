using UnityEngine;
using System;

public abstract class AbstractCursor : MonoBehaviour
{
    public struct BoundAction
    {
        public KeyCode keyCode;
        public UnityEngine.Events.UnityAction action;
    };

    protected Ray _lastRay;

    protected RaycastHit _lastRaycastHit;

    public void SetLastState(Ray ray, RaycastHit hit)
    {
        _lastRay = ray;
        _lastRaycastHit = hit;
    }

    public abstract KeyCode KeyCode();

    public abstract void Place();

    public abstract void Use();

    public virtual BoundAction[] Actions()
    {
        return new BoundAction[] { };
    }
}

public class GameCursor : MonoBehaviour
{
    private enum CursorEnum
    {
        Grab,
        Cut,
        HandlePlacer
    };

    [SerializeField]
    private AbstractCursor[] _cursors;

    private AbstractCursor _currentCursor;


    [SerializeField]
    private LayerMask _layerMask;

    // Start is called before the first frame update
    void Start()
    {
        // _cursor.transform.localScale = Vector3.one * 0.2f;
        Cursor.lockState = CursorLockMode.Locked;
        if (_cursors.Length > 0)
            _currentCursor = _cursors[0];
    }

    // Update is called once per frame
    void Update()
    {
        Ray screenRay = Camera.main.ScreenPointToRay(new Vector3(Screen.width / 2, Screen.height / 2, 0f));
        screenRay.origin += screenRay.direction * 0.25f;
        Debug.DrawRay(screenRay.origin, screenRay.direction);


        if (Input.anyKeyDown)
        {
            for (int i = 0; i < _cursors.Length; i++)
            {
                if (Input.GetKeyDown(_cursors[i].KeyCode()))
                {
                    _currentCursor.enabled = false;
                    _currentCursor = _cursors[i];
                    _currentCursor.enabled = true;
                }

                foreach (var actions in _cursors[i].Actions())
                {
                    if (Input.GetKeyDown(actions.keyCode))
                        actions.action();
                }
            }
        }

        RaycastHit hit;
        if (Physics.Raycast(screenRay, out hit, Mathf.Infinity, _layerMask, QueryTriggerInteraction.Ignore))
        {
            _currentCursor.SetLastState(screenRay, hit);

            transform.position = hit.point;

            _currentCursor?.Place();
            if (Input.GetMouseButtonDown(0))
                _currentCursor?.Use();
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        GrabbableObject def = other.GetComponent<GrabbableObject>();
        if (def == null) return;
        def.Highlight();
    }

    private void OnTriggerExit(Collider other)
    {
        GrabbableObject def = other.GetComponent<GrabbableObject>();
        if (def == null) return;
        def.Unhighlight();
    }


}
