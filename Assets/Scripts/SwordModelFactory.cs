using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SwordModelFactory : MonoBehaviour
{
    [SerializeField]
    private GameObject[] _swordBlades;

    [SerializeField]
    private GameObject _swordHandle;

    public GameObject CreateBlade(int bladeIndex)
    {
        if (bladeIndex < 0 || bladeIndex >= _swordBlades.Length) return null;

        GameObject bladeObj = Instantiate(_swordBlades[bladeIndex]);
        bladeObj.transform.position = Vector3.zero;
        bladeObj.transform.localScale = Vector3.one;

        return bladeObj;
    }

    public GameObject CreateSword(int bladeIndex)
    {
        if (bladeIndex < 0 || bladeIndex >= _swordBlades.Length) return null;

        GameObject swordObj = new GameObject("Sword");

        GameObject bladeObj = CreateBlade(bladeIndex);
        bladeObj.transform.SetParent(swordObj.transform);
        GameObject handleObj = CreateHandle();
        handleObj.transform.SetParent(swordObj.transform);

        return swordObj;
    }

    public GameObject CreateHandle()
    {
        GameObject handleObj = Instantiate(_swordHandle);
        handleObj.transform.position = Vector3.zero;
        handleObj.transform.localScale = Vector3.one;
        return handleObj;
    }
}
