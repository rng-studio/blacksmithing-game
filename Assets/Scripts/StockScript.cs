using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StockScript : MonoBehaviour
{
    public int iron = 3;
    public int bronze = 0;
    public int steel = 0;
    public int gold = 0;
    public int diamond = 0;        
}
