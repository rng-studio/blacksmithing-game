using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    public GameObject pnj;

    private GameObject[] curPNJ;
    void Update()
    {
        curPNJ = GameObject.FindGameObjectsWithTag("PNJ");
        if(curPNJ.Length == 0)
        {
            Instantiate(pnj, transform.position, Quaternion.identity);
        }
        else
        {
            if (curPNJ.Length < 2)
            {
                if (curPNJ[0].GetComponent<npcScript>().isServed == true)
                {
                    Instantiate(pnj, transform.position, Quaternion.identity);
                }
            }
        }
    }
}
