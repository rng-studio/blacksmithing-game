using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(CharacterController))]
public class PlayerMovement : MonoBehaviour
{
    [SerializeField]
    private float CameraSensivity;
    [SerializeField]
    private float PlayerSpeed;

    private CharacterController _controller;
    private Camera _camera;

    // Start is called before the first frame update
    void Start()
    {
        _controller = GetComponent<CharacterController>();
        _camera = GetComponentInChildren<Camera>();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 axisInput = new Vector2(Input.GetAxisRaw("Horizontal"), Input.GetAxisRaw("Vertical"));
        Vector2 mouseInput = new Vector2(Input.GetAxis("Mouse X"), Input.GetAxis("Mouse Y"));

        transform.Rotate(Vector3.up * mouseInput.x * CameraSensivity * Time.deltaTime);
        _camera.transform.Rotate(Vector3.right * -mouseInput.y * CameraSensivity * Time.deltaTime);
        // _camera.transform.localRotation = Quaternion.Euler(Mathf.Clamp(_camera.transform.localRotation.x, -90f, 90f), 0, 0);

        Vector3 movement = transform.forward * axisInput.y + transform.right * axisInput.x;
        movement.Normalize();

        _controller.Move(movement * PlayerSpeed * Time.deltaTime);
    }
}
