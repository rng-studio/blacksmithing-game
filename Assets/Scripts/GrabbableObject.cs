using UnityEngine;

public class GrabbableObject : MonoBehaviour
{
    private Rigidbody _rigidbody;
    private MeshRenderer _renderer;

    public bool Grabbed { get; private set; }

    private Color _baseColor;

    private void Start()
    {
        _rigidbody = GetComponent<Rigidbody>();
        _renderer = GetComponent<MeshRenderer>();
        _baseColor = _renderer.material.color;
        Grabbed = false;
    }

    public void Grab()
    {
        _rigidbody.isKinematic = true;
        transform.SetParent(Camera.main.transform);
        Grabbed = true;
    }

    public void Ungrab()
    {
        _rigidbody.isKinematic = false;
        transform.SetParent(null);
        Grabbed = false;
    }

    public void Highlight()
    {
        _renderer.material.color = _baseColor * 1.25f;
    }

    public void Unhighlight()
    {
        _renderer.material.color = _baseColor;
    }
}
