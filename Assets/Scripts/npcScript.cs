using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class npcScript : MonoBehaviour
{
    private Transform cible;

    private NavMeshAgent agent;

    public NavMeshWaypoints curWaypoint;

    private int curTarget = 0;

    public bool isServed = false;

    public int value = 100;

    public int indexSprite;

    private CharacterAnimationInterface cai;

    private Collider curCollide;

    void Start()
    {
        cible = GameObject.FindGameObjectWithTag("Player").transform;
        agent = gameObject.GetComponent<NavMeshAgent>();
        cai = gameObject.GetComponent<CharacterAnimationInterface>();
        curWaypoint = GameObject.FindGameObjectWithTag("Waypoint").GetComponent<NavMeshWaypoints>();
        indexSprite = Random.Range(0, 3);
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(curWaypoint.transform.position);
        CheckMovement();
    }

    void CheckMovement()
    {
        if(agent.velocity.magnitude > 0)
        {
            cai.Walk();
        }
        if(agent.velocity.magnitude == 0)
        {
            cai.StopWalk();
        }
    }

    public void Service()
    {
        if(agent.velocity.magnitude == 0)
        {
            curWaypoint = curWaypoint.nextWaypoint;
            isServed = true;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        if (curWaypoint.transform.position == other.transform.position)
        {
            if (!curWaypoint.isLast)
            {
                if (curWaypoint.isShop && (isServed == false))
                {

                }
                else
                {
                    curWaypoint = curWaypoint.nextWaypoint;
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
