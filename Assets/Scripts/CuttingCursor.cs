using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
[RequireComponent(typeof(MeshRenderer))]
public class CuttingCursor : AbstractCursor
{
    private MeshRenderer _renderer;

    // Start is called before the first frame update
    void Start()
    {
        _renderer = GetComponent<MeshRenderer>();
        _renderer.enabled = false;
    }

    public override BoundAction[] Actions()
    {
        BoundAction rotatePlane = new BoundAction
        {
            keyCode = UnityEngine.KeyCode.E,
            action = delegate
            {
                transform.Rotate(Vector3.right * 15);
            }
        };
        return new BoundAction[]
        {
            rotatePlane
        };
    }

    public override KeyCode KeyCode()
    {
        return UnityEngine.KeyCode.T;
    }

    public override void Place()
    {
        DeformableMesh def = _lastRaycastHit.collider.GetComponent<DeformableMesh>();
        _renderer.enabled = def  == null ? false : true;
    }

    public override void Use()
    {
        DeformableMesh def = _lastRaycastHit.collider.GetComponent<DeformableMesh>();
        if (def == null) return;
        def.Cut(transform.position, Vector3.Cross((transform.position - _lastRay.origin).normalized, transform.up).normalized);
    }

    private void OnDisable()
    {
        _renderer.enabled = false;
    }
}
