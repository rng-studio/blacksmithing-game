using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tetrahedralization : MonoBehaviour
{
    const int BCC_SPLIT = 2;
    const float HALF_BCC_SPLIT = BCC_SPLIT / 2f;


    private Mesh _mesh;

    private List<Vector3> _vertices;
    private int _bccExtents;

    private void Start()
    {
        _mesh = GetComponent<MeshFilter>()?.mesh ?? null;
        if (_mesh == null)
        {
            Debug.LogError($"[Tetrahedralization] No mesh on the game object named {gameObject.name}");
            return;
        }

        Tetrahedralize();
    }

    private void Update()
    {
        int midpointOffset = _bccExtents * _bccExtents * _bccExtents;
        int midpointStride = _bccExtents - 1;
        for (int x = 0; x < _bccExtents; x++)
        {
            for (int y = 0; y < _bccExtents; y++)
            {
                for (int z = 0; z < _bccExtents; z++)
                {
                    int index = _bccExtents * _bccExtents * z + _bccExtents * y + x;
                    //if (x < _bccExtents - 1)
                    //    Debug.DrawLine(transform.position + _vertices[index], transform.position + _vertices[index + 1]);
                    //if (y < _bccExtents - 1)
                    //    Debug.DrawLine(transform.position + _vertices[index], transform.position + _vertices[index + _bccExtents]);
                    //if (z < _bccExtents - 1)
                    //    Debug.DrawLine(transform.position + _vertices[index], transform.position + _vertices[index + _bccExtents * _bccExtents]);
                    if (x < midpointStride && y < midpointStride && z < midpointStride)
                    {
                        int midpointIndex = midpointStride * midpointStride * z + midpointStride * y + x;
                        Debug.DrawLine(transform.position + _vertices[index], transform.position + _vertices[midpointOffset + midpointIndex]);
                        Debug.DrawLine(transform.position + _vertices[index + 1], transform.position + _vertices[midpointOffset + midpointIndex]);
                        Debug.DrawLine(transform.position + _vertices[index + _bccExtents], transform.position + _vertices[midpointOffset + midpointIndex]);
                        // Debug.DrawLine(transform.position + _vertices[index + _bccExtents * _bccExtents], transform.position + _vertices[midpointOffset + midpointIndex]);
                        // Debug.DrawLine(transform.position + _vertices[index + _bccExtents * _bccExtents + 1], transform.position + _vertices[midpointOffset + midpointIndex]);
                        // Debug.DrawLine(transform.position + _vertices[index + _bccExtents * _bccExtents + _bccExtents], transform.position + _vertices[midpointOffset + midpointIndex]);
                        // Debug.DrawLine(transform.position + _vertices[index + _bccExtents * _bccExtents + _bccExtents], transform.position + _vertices[midpointOffset + midpointIndex]);
                    }
                }
            }
        }
    }

    private float SurfaceDistance(Vector3 point)
    {
        float distance = 0.0f;

        distance = point.magnitude;

        return distance;
    }

    struct Tetrahedra
    {
        public uint a;
        public uint b;
        public uint c;
        public uint d;
    }

    private void Tetrahedralize()
    {
        Bounds bounds = _mesh.bounds;

        List<Tetrahedra> tetrahedras = new List<Tetrahedra>();
        List<Vector3> vertices = new List<Vector3>();

        int bccExtents;
        CreateBCCLattice(bounds, out vertices, out bccExtents);
        _vertices = vertices;
        _bccExtents = bccExtents;
        //CreateTetrahedrasFromBounds(ref tetrahedras, ref vertices);
    }

    private void CreateBCCLattice(Bounds bounds, out List<Vector3> vertices, out int bccExtents)
    {
        vertices = new List<Vector3>();

        float bccSize = 2.1f * Mathf.Max(bounds.extents.x, bounds.extents.y, bounds.extents.z) / BCC_SPLIT;
        for (float z = -HALF_BCC_SPLIT; z <= HALF_BCC_SPLIT; z++)
            for (float y = -HALF_BCC_SPLIT; y <= HALF_BCC_SPLIT; y++)
                for (float x = -HALF_BCC_SPLIT; x <= HALF_BCC_SPLIT; x++)
                    vertices.Add(new Vector3(x, y, z) * bccSize);

        for (float z = -HALF_BCC_SPLIT; z < HALF_BCC_SPLIT; z++)
            for (float y = -HALF_BCC_SPLIT; y < HALF_BCC_SPLIT; y++)
                for (float x = -HALF_BCC_SPLIT; x < HALF_BCC_SPLIT; x++)
                    vertices.Add(new Vector3(x + 0.5f, y + 0.5f, z + 0.5f) * bccSize);

        bccExtents = BCC_SPLIT + 1;
    }

    private void CreateTetrahedrasFromBounds(ref List<Tetrahedra> tetrahedras, ref List<Vector3> vertices)
    {
        int rowStride = BCC_SPLIT + 1;
        int midpointOffset = rowStride * rowStride * rowStride;


    }
}
