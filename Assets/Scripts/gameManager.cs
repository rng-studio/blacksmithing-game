using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;


public class gameManager : MonoBehaviour
{
    private TextMeshProUGUI text;
    private TextMeshProUGUI ironText;
    private TextMeshProUGUI bronzeText;
    private TextMeshProUGUI steelText;
    private TextMeshProUGUI goldText;
    private TextMeshProUGUI diamondText;

    //public AudioClip clip;
    //public AudioSource source;

    public Canvas shopMenu;
    public Canvas pauseMenu;

    private GameObject player;

    public GameObject stock;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player");

        text = GameObject.FindGameObjectWithTag("GoldUI").GetComponent<TextMeshProUGUI>();
        ironText = GameObject.FindGameObjectWithTag("IronUI").GetComponent<TextMeshProUGUI>();
        bronzeText = GameObject.FindGameObjectWithTag("BronzeUI").GetComponent<TextMeshProUGUI>();
        steelText = GameObject.FindGameObjectWithTag("SteelUI").GetComponent<TextMeshProUGUI>();
        goldText = GameObject.FindGameObjectWithTag("GoldOreUI").GetComponent<TextMeshProUGUI>();
        diamondText = GameObject.FindGameObjectWithTag("DiamondUI").GetComponent<TextMeshProUGUI>();
        shopMenu.gameObject.SetActive(false);

        pauseMenu.gameObject.SetActive(false);

        //source = GetComponent<AudioSource>();
    }

    void Update()
    {
        Cursor.visible = shopMenu.gameObject.activeSelf;
        if (Input.GetKeyDown(KeyCode.Tab) && !pauseMenu.gameObject.activeSelf)
        {
            shopMenu.gameObject.SetActive(!shopMenu.gameObject.activeSelf);

        }

        if (shopMenu.gameObject.activeSelf || pauseMenu.gameObject.activeSelf)
        {
            Cursor.lockState = CursorLockMode.Confined;
        }
        else
        {
            Cursor.lockState = CursorLockMode.Locked;
        }

        if (pauseMenu.gameObject.activeSelf)
        {
            Cursor.visible = true;
            Time.timeScale = 0;
        }


        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetKeyDown(KeyCode.P))
        {
            pauseMenu.gameObject.SetActive(true);
        }


        text.SetText("Gold : " + player.GetComponent<PlayerScript>().money);
        ironText.SetText(" : " + stock.GetComponent<StockScript>().iron);
        bronzeText.SetText(" : " + stock.GetComponent<StockScript>().bronze);
        steelText.SetText(" : " + stock.GetComponent<StockScript>().steel);
        goldText.SetText(" : " + stock.GetComponent<StockScript>().gold);
        diamondText.SetText(" : " + stock.GetComponent<StockScript>().diamond);


        //if(Input.GetKeyDown(KeyCode.A))
        //{
        //    source.Play(0);
        //}

    }

    public void quitShop()
    {
        shopMenu.gameObject.SetActive(false);
    }

    public void returnMenu()
    {
        Time.timeScale = 1f;
        SceneManager.LoadScene("MainMenu");
    }

    public void resume()
    {
        Time.timeScale = 1f;
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        pauseMenu.gameObject.SetActive(false);
    }



    public void addIron()
    {
        if(player.GetComponent<PlayerScript>().money >= 80)
        {
            stock.GetComponent<StockScript>().iron += 1;
            player.GetComponent<PlayerScript>().money -= 80;
        }
    }
    public void addBronze()
    {
        if (player.GetComponent<PlayerScript>().money >= 150)
        {
            stock.GetComponent<StockScript>().bronze += 1;
            player.GetComponent<PlayerScript>().money -= 150;
        }
    }
    public void addSteel()
    {
        if (player.GetComponent<PlayerScript>().money >= 210)
        {
            stock.GetComponent<StockScript>().steel += 1;
            player.GetComponent<PlayerScript>().money -= 210;
        }
    }
    public void addGold()
    {
        if (player.GetComponent<PlayerScript>().money >= 270)
        {
            stock.GetComponent<StockScript>().gold += 1;
            player.GetComponent<PlayerScript>().money -= 270;
        }
    }
    public void addDiamond()
    {
        if (player.GetComponent<PlayerScript>().money >= 400)
        {
            stock.GetComponent<StockScript>().diamond += 1;
            player.GetComponent<PlayerScript>().money -= 400;
        }
    }

}
