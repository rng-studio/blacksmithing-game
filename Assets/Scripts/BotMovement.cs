using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class BotMovement : MonoBehaviour
{
    private Transform cible;

    private NavMeshAgent agent;

    private GameObject[] WPs;

    private int curTarget = 0;

    private bool isServed = false;

    void Start()
    {
        cible = GameObject.FindGameObjectWithTag("WaitingZone").transform;
        agent = gameObject.GetComponent<NavMeshAgent>();
        WPs = GameObject.FindGameObjectsWithTag("Waypoint");
        for (int i = 0; i < WPs.Length; i++)
        {
            Debug.Log("Waypoint " + i + " X : " + WPs[i].transform.position.x + " Z : " + WPs[i].transform.position.z);
        }
    }

    // Update is called once per frame
    void Update()
    {
        agent.SetDestination(WPs[curTarget].transform.position);
        Debug.Log("Current Target : " + curTarget);
        Debug.Log("Total Waypoints : " + WPs.Length);
    }

    private void OnTriggerEnter(Collider other)
    {
        if(WPs[curTarget].transform.position == other.transform.position)
        {
            if(curTarget != WPs.Length - 1)
            {
                if (curTarget == 2 && isServed == false) { }
                else
                {
                    curTarget++;
                }
            }
            else
            {
                Destroy(gameObject);
            }
        }
    }
}
