using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityEngine.Audio;

public class MenuInterraction : MonoBehaviour
{
    public AudioMixer audioMixer;
    public Slider volumeSlider;
    private float sliderValue;


    public TMPro.TMP_Dropdown drop_res;
    Resolution[] resolutions;

    private void Start()
    {
        List<Resolution> tmpres = new List<Resolution>();
        foreach (var res in Screen.resolutions)
        {
            if (res.refreshRate == Screen.currentResolution.refreshRate)
            {
                tmpres.Add(res);
            }
        }
        resolutions = tmpres.ToArray();
        drop_res.ClearOptions();

        List<string> resolutions_string = new List<string>();

        int currResolution = 0;
        for (int i = 0; i < resolutions.Length; i++)
        {
            string option = resolutions[i].width + "x" + resolutions[i].height + " - " + resolutions[i].refreshRate + " Hz";
            resolutions_string.Add(option);

            if (resolutions[i].width == Screen.currentResolution.width && resolutions[i].height == Screen.currentResolution.height)
            {
                currResolution = i;
            }
        }
       
        drop_res.AddOptions(resolutions_string);
        drop_res.value = currResolution;
        drop_res.RefreshShownValue();
    }
    private void Update()
    {
        //Debug.Log(resolutionButton.value);
        sliderValue = volumeSlider.value;
        audioMixer.SetFloat("Volume", sliderValue);
        Debug.Log(sliderValue);
    

    }

    public void Play ()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex + 1);
        Time.timeScale = 1;

    }

    public void Quit ()
    {
        Application.Quit();
    }
    public void SetResolution(int index)
    {
        Resolution newRes = resolutions[index];
        Screen.SetResolution(newRes.width, newRes.height, Screen.fullScreen);
    }

}
