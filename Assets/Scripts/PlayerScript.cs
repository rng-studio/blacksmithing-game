using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerScript : MonoBehaviour
{

    public Camera playerCam;

    public int money = 0;

    public GameObject image;

    private Image sprite;

    public Sprite[] bladeSprite;

    private CharacterController _player;

    public float moveSpeed = 5f;

    public int curPattern;

    void Start()
    {
        _player = gameObject.GetComponent<CharacterController>();
        sprite = image.GetComponent<Image>();
        sprite.enabled = false;
    }

    void Update()
    {
        PlayerInteraction();
        PlayerMovement();
    }

    void PlayerMovement()
    {
        Vector2 moveAxis = new Vector2(Input.GetAxisRaw("Vertical"), Input.GetAxisRaw("Horizontal"));
        Vector3 deplacement = transform.forward * moveAxis.x + transform.right * moveAxis.y + transform.up * Physics.gravity.y;
        deplacement.Normalize();
        _player.Move(deplacement * moveSpeed * Time.deltaTime);
    }

    void PlayerInteraction()
    {
        if (Input.GetKeyDown(KeyCode.F))
        {
            Ray ray = new Ray(playerCam.transform.position, playerCam.transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 7f))
            {
                if (hit.transform.gameObject.CompareTag("PNJ"))
                {
                    if (!hit.transform.gameObject.GetComponent<npcScript>().isServed)
                    {
                        sprite.enabled = true;
                        curPattern = hit.transform.gameObject.GetComponent<npcScript>().indexSprite;
                        sprite.sprite = bladeSprite[curPattern];
                    }
                }
            }
        }
        if (Input.GetButtonDown("Fire1"))
        {
            Ray ray = new Ray(playerCam.transform.position, playerCam.transform.forward);
            RaycastHit hit;
            if (Physics.Raycast(ray, out hit, 7f))
            {
                if (hit.transform.gameObject.CompareTag("PNJ"))
                {
                    hit.transform.gameObject.GetComponent<npcScript>().Service();
                    money += hit.transform.gameObject.GetComponent<npcScript>().value;
                    hit.transform.gameObject.GetComponent<npcScript>().value = 0;
                    sprite.enabled = false;
                }
            }
        }
        if(Input.GetKeyDown(KeyCode.G))
        {

        }
    }
}