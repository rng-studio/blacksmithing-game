// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/OutlineShader"
{
    Properties
    {
        _OutlineWidth ("Outline Width", Range(0, 0.1)) = 0.1
        _OutlineColor ("Outline Color", Color) = (1, 1, 1, 1)
    }

    CGINCLUDE

    #include "UnityCG.cginc"

    struct appdata
    {
        float4 vertex : POSITION;
        float3 normal : NORMAL;
    };

    struct v2f
    {
        float4 pos : SV_POSITION;
    };

    float _OutlineWidth;
    float4 _OutlineColor;


    v2f vert(appdata v)
    {
        v2f o;

        o.pos = UnityObjectToClipPos(v.vertex + v.normal * _OutlineWidth);

        return o;
    }
    ENDCG

    SubShader
    {
        Tags
        { 
            "Queue" = "Transparent"
            "RenderType" = "Transparent"
        }

        Pass
        {
            ZWrite Off
            ZTest LEqual
            ColorMask 0
            Stencil {
                Ref 1
                Pass Replace
            }
        }

        Pass
        {
            ZWrite Off
            ZTest Always
            Stencil {
                Ref 1
                Comp NotEqual
            }

            CGPROGRAM
            #pragma vertex vert
            #pragma fragment frag

            half4 frag(v2f i) : COLOR
            {
                return _OutlineColor + sin(_Time.z * 4) / 12 * half4(1, 1, 1, 0);
            }

            ENDCG
        }
    }
}
